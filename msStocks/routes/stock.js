'use strict'

module.exports = function(app) {
    var stock = require('../controllers/stock');
    var request = require('request');
    var requestOptions = {
        'url': 'https://api.tiingo.com/tiingo/fx/top?tickers=audusd,eurusd&token=29c98a2906d0cb6f99ad3226e4377d0769e45d32',
        'headers': {
            'Content-Type': 'application/json'
            }
    }
    app.route('/stocks')
    .get(stock.ListStocks)
    app.route('/forex')
    .get(stock.ListForex)
        //  console.log(response.body);
};

