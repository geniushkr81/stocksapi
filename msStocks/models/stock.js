'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var StockSchema = new Schema({
    userId: {
        type: String,
        required: true,
      },
      ticker: {
        type: String,
        required: true,
      },
      quantity: {
        type: Number,
        required: true,
      },
      price: {
        type: Number,
        required: true,
      },
      takeprofit: {type: Number},
      stoplost: {type: Number},
    date: {
        type: Date,
        default: Date.now(),
      },
});

module.exports = mongoose.model('Stock', StockSchema);