const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const port = process.argv.slice(2)[0];
const app = express();
var mongoose = require('mongoose');
Stock = require('./models/stock');

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost:27017/stockmarket').then(()=>{
    console.log("DB Connect Success ...");
    app.use(bodyParser.urlencoded({extended:true}));
    app.use(bodyParser.json());
    var routes = require('./routes/stock');
    routes(app);
    app.listen(8083, () => {
        console.log(`Stocks service listening on port ${8083}`);
    });

})
.catch(err => console.log(err));
