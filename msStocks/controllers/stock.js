'use strict'
var mongoose = require('mongoose');
Stock = mongoose.model('Stock');

var request = require('request');
var requestOptions = {
    'url': 'https://api.tiingo.com/tiingo/fx/top?tickers=audusd,eurusd&token=29c98a2906d0cb6f99ad3226e4377d0769e45d32',
    'headers': {
        'Content-Type': 'application/json'
        }
}
exports.ListForex = function(req,res) {
    request(requestOptions, function(error,response,body){
        console.log(body);
        // send forex data from API tiingo
        res.send(body);
    })
};
exports.ListStocks = function(req, res) {
    Stock.find().then((data) => {
        console.log(data);
        res.send(data);
    });
};